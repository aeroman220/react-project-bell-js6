import React from 'react';
import Menu from '../components/menu/Menu';
import MenuItem from '../components/menu/MenuItem';
import Modal from '../components/modal/modal'

export default class Layout extends React.Component
{
    constructor(props)
    {
        super(props);
        this.brand = 'React/JS BELL';
    }

    isActive(href)
    {
        return window.location.pathname === href;
    }

    render(){
        return (
            <div>
                <Modal />
                <Menu brand={this.brand}>
                    <MenuItem href="/" active={this.isActive('/')}>
                        Главная
                    </MenuItem>
                    <MenuItem href="/companies" active={this.isActive('/companies')}>
                        Компании
                    </MenuItem>
                    <MenuItem href="/departments" active={this.isActive('/departments')}>
                        Департаменты
                    </MenuItem>
                    <MenuItem href="/users" active={this.isActive('/users')}>
                        Пользователи
                    </MenuItem>
                </Menu>
            <div className="container">
                <div>

                </div>
            <div className="row">
                <div className="col-xs-12">
                    {this.props.children}
                </div>
            </div>
        </div>
                <footer>
                    <hr/>
                    &copy; 2018
                </footer>
            </div>);
    }
}