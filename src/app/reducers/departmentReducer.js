import * as ConstantDepartments from '../constants/departmentsConstans';
import merge from 'lodash/merge';

export function departmentReducer(state = {departments: [], is_loading: false}, action) {
    switch(action.type) {
        case ConstantDepartments.EDIT_DEP: {
            const index = state.departments.findIndex(department => department.id === action.id);
            state.departments[index].title = action.title;
            state.departments[index].phoneDep = action.phoneDep;
            return Object.assign({}, state, {
                departments: state.departments
            })
        }
        case ConstantDepartments.GET_DEPS_PENDING: {
            state = {...state, is_loading:true};
            break;
        }
        case ConstantDepartments.GET_DEPS_FULFILLED: {
            state = {...state, is_loading: false, departments: action.payload.data};
            break;
        }
        case ConstantDepartments.GET_DEPS_REJECTED: {
            state = {...state, is_loading: false, error_message: action.payload.message};
            break;
        }
        case ConstantDepartments.DELETE_DEP: {
            state.departments = state.departments.filter(
                department => department.id !== action.id
            );
            return Object.assign({}, state, {
                departments: state.departments
            })
        }
        case ConstantDepartments.ADD_DEP: {
            state = {
                ...state,
                is_loading: false,
                departments: [...state.departments, action.payload]
            };
            break;
        }
    }
    return state;
}