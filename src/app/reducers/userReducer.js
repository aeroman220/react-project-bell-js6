import * as ConstantUsers from '../constants/usersConstans';
import merge from 'lodash/merge';

export function userReducer(state = {users: [], is_loading: false}, action) {
    switch(action.type) {
        case ConstantUsers.GET_USERS_PENDING: {
            state = {...state, is_loading:true};
            break;
        }
        case ConstantUsers.GET_USERS_FULFILLED: {
            state = {...state, is_loading: false, users: action.payload.data};
            break;
        }
        case ConstantUsers.GET_USERS_REJECTED: {
            state = {...state, is_loading: false, error_message: action.payload.message};
            break;
        }
        case ConstantUsers.DELETE_USER: {
            let index = null;
            state.users.forEach((value, i) => {
                if(parseInt(value.id) === parseInt(action.payload)) {
                    index = i;
                }
            });
            if(index !== null)
            {
                state = merge({}, state);
                state.users.splice(index, 1);
            }
            break;
        }
        case ConstantUsers.ADD_USER: {
            state = {
                ...state,
                is_loading: false,
                users: [...state.users, action.payload]
            };
            break;
        }
    }
    return state;
}