import * as ConstantCompanies from '../constants/companiesConstants';


export function companyReducer(state = {companies: [], is_loading: false}, action) {
	switch(action.type) {
		case ConstantCompanies.EDIT_COMP: {
			const index = state.companies.findIndex(company => company.id === action.id);
			state.companies[index].title = action.title;
            state.companies[index].address = action.address;
            state.companies[index].INN = action.INN;
			return Object.assign({}, state, {
				companies: state.companies
			})
		}
		case ConstantCompanies.GET_COMPS_PENDING: {
			state = {...state, is_loading:true};
			break;
		}
		case ConstantCompanies.GET_COMPS_FULFILLED: {
			state = {...state, is_loading: false, companies: action.payload.data};
			break;
		}
		case ConstantCompanies.GET_COMPS_REJECTED: {
			state = {...state, is_loading: false, error_message: action.payload.message};
			break;
		}
		case ConstantCompanies.DELETE_COMP: {
			state.companies = state.companies.filter(
				company => company.id !== action.id
			);
			return Object.assign({}, state, {
				companies: state.companies
			})
		}
        case ConstantCompanies.ADD_COMP: {
            state = {
                ...state,
                is_loading: false,
                companies: [...state.companies, action.payload]
            };
            break;
        }
	}
	return state;
}