import React from 'react';
import {Link} from 'react-router';

export default class User extends React.Component
{
    render()
    {
        const { name, address, staff } = this.props
        return (
            <div className="card border-secondary mb-3">
                <div className="card-body text-secondary">
                    <p>ФИО: {this.props.name}</p>
                    <p>Адрес: {this.props.address}</p>
                    <p>Должность: {this.props.staff}</p>
                </div>
            </div>
        );
    }
}
