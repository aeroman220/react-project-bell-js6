import React from 'react';
import User from './User';

export default class UserList extends React.Component {
    render()
    {
        if(!this.props.users.length) {
            return null;
        }

        let users = this.props.users.map((department, id) => {
            return <User key={id} {...department}/>
        });

        return (
            <div>
                <h1>Сотрудники</h1>
                <div>
                    {users}
                </div>
            </div>
        );
    }
}



/*
import React from 'react';
import axios from 'axios';
import User from './User';

export default class UsersList extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            users: []
        };

        axios
            .get('http://localhost:3000/users')
            .then((response) => {
                this.setState({users: response.data});
            });
    }

    render()
    {
        if(!this.state.users.length){
            return null;
        }

        let users = this.state.users.map((user, id) => {
            return <User key={id} {...user} />
        });

        return (
            <div>
                <h1>Сотрудники</h1>
                {users}
            </div>
        );
    }
}*/
