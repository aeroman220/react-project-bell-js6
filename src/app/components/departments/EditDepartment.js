import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {closeModal} from '../../actions/modalAction'
import Input from '../../components/ui/input/index'

class EditDepartment extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            id: this.props.id,
            titleDep: this.props.titleDep,
            phoneDep: this.props.phoneDep,
            errors: {
                id: '',
                titleDep: '',
            }
        };
        this.close = this.close.bind(this);
        this.save = this.save.bind(this);
        this.changeTitleDep = this.changeTitleDep.bind(this);
        this.changePhoneDep = this.changePhoneDep.bind(this);

    }

    changeTitleDep(titleDep){
        this.setState({titleDep})
    }

    changePhoneDep(phoneDep) {
        this.setState({phoneDep})
    }
    save(){
        const { id, titleDep, phoneDep } = this.state;
        const errorTitle = 'Поле не должно быть пустым';
        const errors = { titleDep: '', phoneDep: ''};
        if (titleDep === '') {errors.titleDep = errorTitle}
        if (phoneDep === '') {errors.phoneDep = errorTitle}
        if (errors.titleDep || errors.phoneDep) {
            this.setState({ errors });
            return
        }
        this.setState({ errors })
        this.props.dispatch(this.props.onSave({ id, titleDep, phoneDep }));
        this.close()
    }
    close(){
        this.props.dispatch(closeModal())
    }
    render(){
        return(
            <div>
                <div className='modal-body'>
                    <p><b>ID:</b>{ this.state.id }</p>
                    <Input
                        value={ this.state.titleDep}
                        onChange={this.changeTitleDep}
                        error={this.state.errors.titleDep}
                    />
                    <Input
                        value={ this.state.phoneDep}
                        onChange={this.changePhoneDep}
                        error={this.state.errors.phoneDep}
                    />
                </div>
                <div className='modal-footer'>
                    <button className='btn btn-outline-success' onClick={ this.save }>Сохранить</button>
                    <button className='btn btn-outline-default' onClick={ this.close }>Закрыть</button>

                </div>
            </div>
        )
    }
}

EditDepartment.propTypes = {
    dispatch: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    titleDep: PropTypes.string.isRequired,
    phoneDep: PropTypes.string.isRequired,
    onSave: PropTypes.func.isRequired
};

function mapStateToProps(state) {
    return {}
}

export default connect(mapStateToProps)(EditDepartment)
