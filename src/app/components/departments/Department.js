import React from 'react';
import {Link} from 'react-router';
import { connect } from 'react-redux';
import EditDepartment  from './EditDepartment';
import DelDepartment  from './DelDepartment';
import { openModal } from '../../actions/modalAction';
import { editDepartment, delDepartment } from '../../actions/departmentActions'


class Department extends React.Component {
    constructor(props){
        super(props);
        //this.add = this.del.bind(this)
        this.edit = this.edit.bind(this);
        this.remove = this.remove.bind(this)
    }

    edit() {
        const { id, titleDep, phoneDep} = this.props;
        this.props.dispatch(openModal({
            title: 'Редактирование',
            content: <EditDepartment id={ id }
                                  titleDep = {titleDep}
                                  phoneDep={ phoneDep }
                                  onSave={ editDepartment }/>,
        }))
    }

    remove() {
        const { id, titleDep } = this.props;
        this.props.dispatch(openModal({
            title: 'Удалить элемент',
            content: <DelDepartment id={ id }
                                 titleDep = {titleDep}
                                 onSuccess={ delDepartment}/>,
        }))
    }

    render()
    {
        const { id, titleDep, phoneDep} = this.props;
        return (
            <div className="card border-secondary mb-3">
                <h3 className="card-header">
                    <Link to={`/departments`}>
                        {titleDep}
                    </Link>
                </h3>
                <div className="card-body text-secondary">
                    <div>Телефон департамента: {phoneDep}</div>
                    <button className='btn btn-outline-primary' onClick={ this.edit }>Редактировать</button>
                    <button className='btn btn-outline-danger' onClick={ this.remove }>Удалить</button>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {}
}

export default connect(mapStateToProps)(Department)
