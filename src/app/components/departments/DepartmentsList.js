import React from 'react';
import Department from './Department';

export default class DepartmentList extends React.Component {
    render()
    {
        if(!this.props.departments.length) {
            return null;
        }

        let departments = this.props.departments.map((department, id) => {
                return <Department key={id} {...department}/>
        });

        return (
            <div>
                <h1>Департаменты</h1>
                <div>
                    {departments}
                </div>
            </div>
        );
    }
}