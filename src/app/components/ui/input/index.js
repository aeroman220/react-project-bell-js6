import React from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

export default class Input extends React.Component {
    constructor(props) {
        super(props);
        const { value } = this.props;
        this.state = { value };
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event) {
        const { value } = event.target;
        this.props.onChange(value);
        this.setState({ value })
    }
    render(){
        const divClasses = classNames({
            'form-group': true,
            'has-error': !!this.props.error
        });
        const { id, placeholder } = this.props;
        return(
            <div className={ divClasses }>
                <label htmlFor={ id }>{ id }</label>
                <input
                    type="text"
                    id = { id }
                    className = 'form-control'
                    value ={ this.state.value }
                    onChange = { this.handleChange }
                    placeholder = { placeholder }
                />
                { this.props.error ? <span className='help-block' >{ this.props.error}</span> : null}
            </div>
        )
    }

}

Input.propTypes = {
    id: PropTypes.number,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    error: PropTypes.string
}