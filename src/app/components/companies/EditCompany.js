import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {closeModal} from '../../actions/modalAction'
import Input from '../../components/ui/input/index'

class EditCompany extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            id: this.props.id,
            title: this.props.title,
            address: this.props.address,
            INN: this.props.INN,
            errors: {
                id: '',
                title: '',
                address: '',

            }
        };
        this.close = this.close.bind(this);
        this.save = this.save.bind(this);
        this.changeTitle = this.changeTitle.bind(this);
        this.changeAddress = this.changeAddress.bind(this);
        this.changeINN = this.changeINN.bind(this);

    }

    changeTitle(title){
        this.setState({title})
    }
    changeAddress(address) {
        this.setState({address})
    }
    changeINN(INN) {
        this.setState({INN})
    }
    save(){
        const { id, title, address, INN } = this.state;
        const errorTitle = 'Поле не должно быть пустым';
        const errors = { title: '', address: '', INN: ''};
        if (title === '') {errors.title = errorTitle}
        if (address === '') {errors.address = errorTitle}
        if (INN === '') {errors.INN = errorTitle}
        if (errors.title || errors.address || errors.INN) {
            this.setState({ errors })
            return
        }
        this.setState({ errors })
        this.props.dispatch(this.props.onSave({ id, title, address, INN }));
        this.close()
    }
    close(){
        this.props.dispatch(closeModal())
    }
    render(){
        return(
            <div>
                <div className='modal-body'>
                    <p><b>ID:</b>{ this.state.id }</p>
                    <Input
                        value={ this.state.title}
                        onChange={this.changeTitle}
                        error={this.state.errors.title}
                    />
                    <Input
                        value={ this.state.address}
                        onChange={this.changeAddress}
                        error={this.state.errors.address}
                    />
                    <Input
                        value={ this.state.INN}
                        onChange={this.changeINN}
                        error={this.state.errors.INN}
                    />
                </div>
                <div className='modal-footer'>
                    <button className='btn btn-outline-success' onClick={ this.save }>Сохранить</button>
                    <button className='btn btn-outline-default' onClick={ this.close }>Закрыть</button>

                </div>
            </div>
        )
    }
}

EditCompany.propTypes = {
    dispatch: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    INN: PropTypes.string.isRequired,
    onSave: PropTypes.func.isRequired
}


function mapStateToProps(state) {
    return {}
}


export default connect(mapStateToProps)(EditCompany)
