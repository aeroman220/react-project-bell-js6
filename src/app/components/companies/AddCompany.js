import React from 'react';

export default class AddCompany extends React.Component {
	render() {
		return (
				<div>
					<h2>Добавить компанию</h2>
					<form action="#" id="addCompanyForm">
						<div className="form-group">
							<label htmlFor="idUser">Id пользователя</label>
							<input type="text" className="form-control" id="idCompany" placeholder="New id"/>
						</div>
						<div className="form-group">
							<label htmlFor="idUser">Заголовок</label>
							<input type="text" className="form-control" id="companyTitle" placeholder="Название компании"/>
						</div>
						<div className="form-group">
							<label htmlFor="idUser">Текст</label>
							<input type="text" className="form-control" id="address" placeholder="адрес"/>
						</div>
						<div className="form-group">
							<label htmlFor="idUser">Заголовок</label>
							<input type="text" className="form-control" id="INN" placeholder="ИНН"/>
						</div>
						<button className="btn btn-primary">Добавить</button>
					</form>
				</div>
			);
	}
}