import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { closeModal } from '../../actions/modalAction'
import { delCompany } from '../../actions/companyActions'

class DelCompany extends React.Component {
    constructor(props){
        super(props);
        this.cancel = this.cancel.bind(this);
        this.deleteComp = this.deleteComp.bind(this);
    }

    deleteComp(){
        this.props.dispatch( this.props.onSuccess(this.props.id))
        this.cancel()
    }

    cancel(){
        this.props.dispatch(closeModal())
    }
    render(){
        return(
            <div>
                <div className='modal-body'>
                    <p>Вы хотите удалить из списка компанию <b>ID:</b>{ this.props.id } - {this.props.title}</p>
                </div>
                <div className='modal-footer'>
                    <button className='btn btn-outline-danger' onClick={ this.deleteComp }>Удалить</button>
                    <button className='btn btn-outline-default' onClick={ this.cancel }>Отмена</button>

                </div>
            </div>
        )
    }
}

DelCompany.propTypes = {
    dispatch: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    onSuccess: PropTypes.func.isRequired
}


function mapStateToProps(state) {
    return {}
}


export default connect(mapStateToProps)(DelCompany)
