import React from 'react';
import {Link} from 'react-router';
import { connect } from 'react-redux';
import EditCompany  from './EditCompany';
import DelCompany  from './DelCompany';
import { openModal } from '../../actions/modalAction';
import { editCompany, delCompany } from '../../actions/companyActions'


class CompanyItem extends React.Component {
    constructor(props){
        super(props);
        //this.add = this.del.bind(this)
        this.edit = this.edit.bind(this);
        this.remove = this.remove.bind(this)
    }

    edit() {
        const { id, title, address, INN} = this.props;
        this.props.dispatch(openModal({
			title: 'Редактирование',
			content: <EditCompany id={ id }
								  title = {title}
								  address={ address }
								  INN={ INN }
								  onSave={ editCompany }/>,
		}))
    }

    remove() {
        const { id, title } = this.props;
        this.props.dispatch(openModal({
            title: 'Удалить элемент',
            content: <DelCompany id={ id }
								  title = {title}
								  onSuccess={ delCompany}/>,
        }))
    }

	render()
	{
		const { id, title, address, INN} = this.props;
		return (
				<div className="card border-secondary mb-3">
					<h3 className="card-header">
						<Link to={`/departments`}>
							{title}
						</Link>
					</h3>
					<div className="card-body text-secondary">
						<div>Адрес компании: {address}</div>
						<div>ИНН: {INN}</div>
						<button className='btn btn-outline-primary' onClick={ this.edit }>Редактировать</button>
						<button className='btn btn-outline-danger' onClick={ this.remove }>Удалить</button>
					</div>
				</div>
			);
	}
}

function mapStateToProps(state) {
    return {}
}

export default connect(mapStateToProps)(CompanyItem)