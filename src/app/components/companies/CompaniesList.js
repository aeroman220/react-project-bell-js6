import React from 'react';
import CompanyItem from './CompanyItem';

export default class CompaniesList extends React.Component
{
	render()
	{
		if(!this.props.companies.length) {
			return null;
		}

		let companies = this.props.companies.map((company, id) => {
			return <CompanyItem key={id} {...company}/>
		});

		return (
				<div>
					<h1>Компании</h1>
					<div>
						{companies}
					</div>
				</div>
			);
	}
}