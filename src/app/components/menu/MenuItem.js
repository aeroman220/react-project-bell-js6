import React from 'react';
import {Link} from 'react-router';
import './MenuItem-style.css'

export default class MenuItem extends React.Component
{
    render()
    {
        return (
            <li className={this.props.active ? 'nav-item active' : 'nav-item'}>
                <Link className='nav-link item' to={this.props.href}>
                    {this.props.children}
                </Link>
            </li>
        );
    }
}