import React from 'react';
import { getCompanies } from '../actions/companyActions';
import CompaniesList from '../components/companies/CompaniesList';
import {connect} from 'react-redux';


class Companies extends React.Component {

    componentWillUnmount() {

    }
    componentDidMount() {
        this.props.dispatch(getCompanies());
    }
    render() {

        if (this.props.is_loading) {
            return <div>Данные загружаются...</div>
        }

        return (
            <div>
                <CompaniesList companies={this.props.companies}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        companies: state.companies.companies,
        is_loading: state.companies.is_loading
    }
}

export default connect(mapStateToProps)(Companies);
