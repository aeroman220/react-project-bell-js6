import React from 'react';
import { getUsers } from '../actions/userActions';
import UsersList from '../components/users/UsersList';
import { connect } from 'react-redux';


class Users extends React.Component {

    componentWillUnmount() {
    }
    componentDidMount() {
        this.props.dispatch(getUsers());
    }

    render() {

        if (this.props.is_loading) {
            return <div>Данные загружаются...</div>
        }

        return (
            <div>
                <UsersList users={this.props.users}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        users: state.users.users,
        is_loading: state.users.is_loading
    }
}

export default connect(mapStateToProps)(Users);
