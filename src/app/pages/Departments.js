import React from 'react';
import {getDepartments} from '../actions/departmentActions';
import DepartmentsList from '../components/departments/DepartmentsList';
import {connect} from 'react-redux';


class Departments extends React.Component {

    componentWillUnmount() {
    }
    componentDidMount() {
        this.props.dispatch(getDepartments());
    }

    render() {
        if (this.props.is_loading) {
            return <div>Данные загружаются...</div>
        }

        return (
            <div>
                <DepartmentsList departments={this.props.departments}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        departments: state.departments.departments,
        is_loading: state.departments.is_loading
    }
}

export default connect(mapStateToProps)(Departments);
