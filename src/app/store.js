import {createStore, combineReducers, applyMiddleware} from 'redux';

import logger from 'redux-logger';
import promise from 'redux-promise-middleware';
import thunk from 'redux-thunk';

import {companyReducer} from './reducers/companyReducer';
import {departmentReducer} from "./reducers/departmentReducer";
import {userReducer} from './reducers/userReducer'
import { modalReducer} from './reducers/modalReducer'

const reducers = combineReducers({
    companies: companyReducer,
    departments: departmentReducer,
    users: userReducer,
    modal: modalReducer
})

const middleware = applyMiddleware(promise(), thunk, logger);

const store = createStore(reducers, middleware);
export default store;