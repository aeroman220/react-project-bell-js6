import {ADD_COMP, EDIT_COMP, GET_COMPS, DELETE_COMP} from '../constants/companiesConstants';
import axios from 'axios';

export function addCompany(company) {
	const { id, title, address, INN } = company;
	return {
		type: ADD_COMP,
        id, title, address, INN
	}
}

export function editCompany(company) {
	const { id, title, address, INN } = company;
    return {
        type: EDIT_COMP,
        id, title, address, INN
    }
}

export function getCompanies(){
	return {
		type: GET_COMPS,
		payload: axios.get('http://localhost:3000/companies')
	};
}

export function delCompany(id) {
	return {
		type: DELETE_COMP,
		id
	}
}