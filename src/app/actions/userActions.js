import {ADD_USER, GET_USERS, DELETE_USER} from '../constants/usersConstans';
import axios from 'axios';

export function addUser(title, id, body) {
    return {
        type: ADD_USER,
        payload: {title, id, body}
    }
}

export function getUsers(){
    return {
        type: GET_USERS,
        payload: axios.get('http://localhost:3000/users')
    };
}

export function deleteUser(idDepartment) {
    return {
        type: DELETE_USER,
        payload: idDepartment
    }
}