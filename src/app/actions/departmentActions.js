import {ADD_DEP, EDIT_DEP, GET_DEPS, DELETE_DEP} from '../constants/departmentsConstans';
import axios from 'axios';

export function addDepartment(id, idComp, titleDep, phoneDep) {
    return {
        type: ADD_DEP,
        payload: {id, idComp, titleDep, phoneDep}
    }
}

export function editDepartment(department) {
    const { id, titleDep, phoneDep } = department;
    return {
        type: EDIT_DEP,
        id, titleDep, phoneDep
    }
}

export function getDepartments(){
    const idComp = 1;
    return {
        type: GET_DEPS,
        payload: axios.get(`http://localhost:3000/departments`,
            {
                params: {
                    idComp: idComp
            }
        })
    };
}

export function delDepartment(id) {
    return {
        type: DELETE_DEP,
        id
    }
}