export const ADD_DEP = 'ADD_DEP';
export const EDIT_DEP = 'EDIT_DEP';
export const GET_DEPS = 'GET_DEPS';
export const GET_DEPS_PENDING = 'GET_DEPS_PENDING';
export const GET_DEPS_FULFILLED = 'GET_DEPS_FULFILLED';
export const GET_DEPS_REJECTED = 'GET_DEPS_REJECTED';
export const DELETE_DEP = "DELETE_DEP";