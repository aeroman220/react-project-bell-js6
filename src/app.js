import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, IndexRoute, browserHistory} from 'react-router';
import 'bootstrap/dist/css/bootstrap.min.css';
import Layout from './app/layouts/Layout';
import MainPage from './app/pages/Main';
import PageNotFound from './app/pages/PageNotFound';
import Users from './app/pages/Users';
import Companies from './app/pages/Companies';
import Departments from './app/pages/Departments';
import {Provider} from 'react-redux';
import store from './app/store';


const app = document.getElementById('app');

ReactDOM.render(<Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/" component={Layout}>
                <IndexRoute component={MainPage}/>
                <Route path="companies" component={Companies}>
                </Route>
                <Route path="/departments" component={Departments}>
                </Route>
                <Route path="/users" component={Users}>
                </Route>
                <Route path="*" component={PageNotFound}/>
            </Route>
        </Router>
    </Provider>,
app);