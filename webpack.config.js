const path = require('path');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const fallbackApi = require('connect-history-api-fallback');

module.exports = {
	entry: './src/app.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'main.js'
	},
	//mode: 'development',

	module: {
		rules: 
		[
			{
				test: /\.js$/,
				loader: "babel-loader",
				exclude: "/node_modules/"
			},
			{
        		test: /\.css$/,
      			use: ['style-loader', 'css-loader']
      		},
            {
                test: /\.less$/,
                use: [
                    'css-loader',
                    'less-loader'
                ]
			}
		]
	},
	plugins: [
	    new BrowserSyncPlugin({
	      host: 'localhost',
	      port: 3000,
	      middleware: fallbackApi(),
	      server: { baseDir: ['dist'] }
	    })
  	]
}